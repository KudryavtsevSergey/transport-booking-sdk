<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Enum;

class RequestMethodEnum extends AbstractEnum
{
    public const GET = 'GET';
    public const HEAD = 'HEAD';
    public const PUT = 'PUT';
    public const POST = 'POST';
    public const PATCH = 'PATCH';
    public const DELETE = 'DELETE';

    public static function getValues(): array
    {
        return [
            self::GET,
            self::HEAD,
            self::PUT,
            self::POST,
            self::PATCH,
            self::DELETE,
        ];
    }
}
