<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Mapper;

use Sun\TransportBookingDto\Request\RequestDtoInterface;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;
use Sun\TransportBookingSdk\Exceptions\TransportBookingRuntimeException;
use Symfony\Component\PropertyInfo\Extractor\ConstructorExtractor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ArrayObjectMapper
{
    private Serializer $serializer;

    public function __construct()
    {
        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();
        $extractor = new PropertyInfoExtractor(
            typeExtractors: [new ConstructorExtractor([$phpDocExtractor]), $phpDocExtractor, $reflectionExtractor]
        );
        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(
                nameConverter: new CamelCaseToSnakeCaseNameConverter(),
                propertyTypeExtractor: $extractor
            ),
            new ArrayDenormalizer(),
        ];
        $this->serializer = new Serializer($normalizers);
    }

    public function serialize(RequestDtoInterface $model): array
    {
        try {
            $data = $this->serializer->normalize($model);
            if (!is_array($data)) {
                throw new TransportBookingRuntimeException('Model was not normalized');
            }
            return $data;
        } catch (ExceptionInterface $e) {
            throw new TransportBookingRuntimeException('Error normalize model to array', $e);
        }
    }

    /**
     * @param array $data
     * @param string $type
     * @return ResponseDtoInterface|ResponseDtoInterface[]
     */
    public function deserialize(array $data, string $type): array|ResponseDtoInterface
    {
        try {
            /** @var ResponseDtoInterface|ResponseDtoInterface[] $result */
            $result = $this->serializer->denormalize($data, $type, null, [
                AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => true,
            ]);
            return $result;
        } catch (ExceptionInterface $e) {
            throw new TransportBookingRuntimeException('Error denormalize array to model', $e);
        }
    }
}
