<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Exceptions;

use RuntimeException;

abstract class AbstractInternalException extends RuntimeException
{

}
