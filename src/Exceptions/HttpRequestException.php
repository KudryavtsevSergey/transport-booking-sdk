<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Exceptions;

use Exception;
use Sun\TransportBookingSdk\Service\HttpClient\HttpResponse;

class HttpRequestException extends Exception
{
    private HttpResponse $response;

    public function __construct(HttpResponse $response)
    {
        $message = sprintf('HTTP request returned status code %s', $response->status());
        parent::__construct($message);
        $this->response = $response;
    }

    public function getResponse(): HttpResponse
    {
        return $this->response;
    }
}
