<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Exceptions;

use Throwable;

class OAuth2CreateTokenException extends AbstractInternalException
{
    public function __construct(Throwable $previous)
    {
        parent::__construct('OAuth2 Create Token Error', 0, $previous);
    }
}
