<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Exceptions;

use Throwable;

class TransportBookingRuntimeException extends AbstractInternalException
{
    public function __construct(string $message, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
}
