<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Exceptions;

use Sun\TransportBookingDto\Response\Error\RequestErrorDto;
use Throwable;

class RestException extends AbstractInternalException
{
    private const ERROR_MESSAGE = 'Request error';

    public function __construct(
        private int $status,
        private ?RequestErrorDto $requestErrorDto,
        Throwable $previous
    ) {
        parent::__construct(self::ERROR_MESSAGE, 0, $previous);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getRequestErrorDto(): ?RequestErrorDto
    {
        return $this->requestErrorDto;
    }
}
