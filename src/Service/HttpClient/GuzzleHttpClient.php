<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\HttpClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\ResponseInterface;
use Sun\TransportBookingSdk\Exceptions\TransportBookingRuntimeException;

class GuzzleHttpClient implements HttpClient
{
    private Client $client;

    public function __construct(string $url, array $headers)
    {
        $this->client = new Client([
            'base_uri' => $url,
            'headers' => $headers,
        ]);
    }

    public function sendRequest(HttpRequest $request): PromiseInterface
    {
        try {
            return $this->client->requestAsync(
                $request->getMethod(),
                $request->buildRoute(),
                $request->getOptions()
            )->then(
                static fn(
                    ResponseInterface $response
                ): HttpResponse => (new HttpResponse($response))->throw(),
                static function(RequestException $e): void {
                    $response = $e->getResponse();
                    if ($response !== null) {
                        (new HttpResponse($response))->throw();
                    }
                    throw new TransportBookingRuntimeException('Send request exception', $e);
                }
            );
        } catch (GuzzleException $e) {
            throw new TransportBookingRuntimeException('Send request guzzle exception', $e);
        }
    }
}
