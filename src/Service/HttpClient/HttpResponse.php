<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\HttpClient;

use Psr\Http\Message\ResponseInterface;
use Sun\TransportBookingSdk\Exceptions\HttpRequestException;

class HttpResponse
{
    private ?array $decoded = null;

    public function __construct(
        private ResponseInterface $response,
    ) {
    }

    public function body(): string
    {
        return (string)$this->response->getBody();
    }

    public function json(string $key = null, ?array $default = null): ?array
    {
        if (!$this->decoded) {
            $this->decoded = (array)json_decode($this->body(), true, flags: JSON_THROW_ON_ERROR);
        }

        if ($key === null) {
            return $this->decoded;
        }

        return $this->decoded[$key] ?? $default;
    }

    public function header(string $header): string
    {
        return $this->response->getHeaderLine($header);
    }

    public function headers(): array
    {
        return $this->response->getHeaders();
    }

    public function status(): int
    {
        return (int)$this->response->getStatusCode();
    }

    public function successful(): bool
    {
        return $this->status() >= 200 && $this->status() < 300;
    }

    public function redirect(): bool
    {
        return $this->status() >= 300 && $this->status() < 400;
    }

    public function failed(): bool
    {
        return $this->serverError() || $this->clientError();
    }

    public function clientError(): bool
    {
        return $this->status() >= 400 && $this->status() < 500;
    }

    public function serverError(): bool
    {
        return $this->status() >= 500;
    }

    /**
     * @return $this
     * @throws HttpRequestException
     */
    public function throw(): self
    {
        if ($this->failed()) {
            throw new HttpRequestException($this);
        }

        return $this;
    }
}
