<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\HttpClient;

use GuzzleHttp\Promise\PromiseInterface;

interface HttpClient
{
    public function sendRequest(HttpRequest $request): PromiseInterface;
}
