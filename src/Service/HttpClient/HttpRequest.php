<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\HttpClient;

use GuzzleHttp\RequestOptions;
use Sun\TransportBookingSdk\Enum\RequestMethodEnum;

class HttpRequest
{
    private const REQUEST_CONTENT_TYPE = 'application/json';

    public function __construct(
        private string $method,
        private string $route,
        private ?string $query,
        private ?string $body,
    ) {
        RequestMethodEnum::checkAllowedValue($method);
    }

    public static function of(string $method, string $route, ?string $query, ?string $body): self
    {
        return new self($method, $route, $query, $body);
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function buildRoute(): string
    {
        $params = $this->query ? sprintf('?%s', $this->query) : '';
        return $this->route . $params;
    }

    private function getBody(): ?string
    {
        return $this->isContainsBody() ? $this->body : null;
    }

    public function getOptions(): array
    {
        $options = [
            RequestOptions::HEADERS => ['Content-Type' => self::REQUEST_CONTENT_TYPE],
        ];
        if ($body = $this->getBody()) {
            $options[RequestOptions::BODY] = $body;
        }
        return $options;
    }

    private function isContainsBody(): bool
    {
        return in_array($this->method, [
            RequestMethodEnum::POST,
            RequestMethodEnum::PUT,
            RequestMethodEnum::PATCH,
        ]);
    }
}
