<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\Request;

use Sun\TransportBookingDto\Request\BodyDtoInterface;
use Sun\TransportBookingDto\Request\QueryDtoInterface;

abstract class AbstractRequest
{
    public function __construct(
        private string $endPoint,
        private ?QueryDtoInterface $query = null,
        private ?BodyDtoInterface $body = null,
    ) {
    }

    public function getEndPoint(): string
    {
        return $this->endPoint;
    }

    public function getQuery(): ?QueryDtoInterface
    {
        return $this->query;
    }

    public function getBody(): ?BodyDtoInterface
    {
        return $this->body;
    }
}
