<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\Request;

use GuzzleHttp\Promise\PromiseInterface;
use Sun\TransportBookingDto\Request\BodyDtoInterface;
use Sun\TransportBookingDto\Request\QueryDtoInterface;
use Sun\TransportBookingDto\Response\Error\RequestErrorDto;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;
use Sun\TransportBookingSdk\Enum\RequestMethodEnum;
use Sun\TransportBookingSdk\Exceptions\HttpRequestException;
use Sun\TransportBookingSdk\Exceptions\RestException;
use Sun\TransportBookingSdk\Mapper\ArrayObjectMapper;
use Sun\TransportBookingSdk\Mapper\JsonObjectMapper;
use Sun\TransportBookingSdk\Service\HttpClient\HttpClient;
use Sun\TransportBookingSdk\Service\HttpClient\HttpRequest;
use Sun\TransportBookingSdk\Service\HttpClient\HttpResponse;
use Throwable;

class RequestService
{
    public function __construct(
        private HttpClient $client,
        private ArrayObjectMapper $arrayObjectMapper,
        private JsonObjectMapper $jsonObjectMapper,
    ) {
    }

    public function sendGetRequest(RestRequest $restRequest): PromiseInterface
    {
        return $this->sendJsonRequest(RequestMethodEnum::GET, $restRequest);
    }

    public function sendPostRequest(RestRequest $restRequest): PromiseInterface
    {
        return $this->sendJsonRequest(RequestMethodEnum::POST, $restRequest);
    }

    public function sendPutRequest(RestRequest $restRequest): PromiseInterface
    {
        return $this->sendJsonRequest(RequestMethodEnum::PUT, $restRequest);
    }

    public function sendDeleteRequest(RestRequest $restRequest): PromiseInterface
    {
        return $this->sendJsonRequest(RequestMethodEnum::DELETE, $restRequest);
    }

    private function sendJsonRequest(string $method, RestRequest $restRequest): PromiseInterface
    {
        $httpRequest = $this->buildHttpRequest($method, $restRequest);
        return $this->sendRequest($httpRequest)->then(function (
            HttpResponse $response
        ) use ($restRequest): array|ResponseDtoInterface|null {
            if ($restRequest->getReturnType() === null) {
                return null;
            }
            return $this->arrayObjectMapper->deserialize($response->json(), $restRequest->getReturnType());
        });
    }

    public function sendFileRequest(FileRequest $fileRequest): PromiseInterface
    {
        $httpRequest = $this->buildHttpRequest(RequestMethodEnum::GET, $fileRequest);
        return $this->sendRequest($httpRequest)->then(static fn(
            HttpResponse $response
        ): string => $response->body());
    }

    private function buildHttpRequest(string $method, AbstractRequest $request): HttpRequest
    {
        $query = $this->buildQuery($request->getQuery());
        $body = $this->buildBody($request->getBody());
        return HttpRequest::of($method, $request->getEndPoint(), $query, $body);
    }

    private function sendRequest(HttpRequest $httpRequest): PromiseInterface
    {
        return $this->client->sendRequest($httpRequest)->otherwise(function (
            Throwable $exception
        ): void {
            if (!$exception instanceof HttpRequestException) {
                throw $exception;
            }
            $json = $exception->getResponse()->json();
            /** @var RequestErrorDto|null $requestErrorDto */
            $requestErrorDto = $json !== null ? $this->arrayObjectMapper->deserialize(
                $json,
                RequestErrorDto::class
            ) : null;
            throw new RestException($exception->getResponse()->status(), $requestErrorDto, $exception);
        });
    }

    private function buildQuery(?QueryDtoInterface $query): ?string
    {
        $params = $query ? $this->arrayObjectMapper->serialize($query) : null;
        return $params ? http_build_query($params) : null;
    }

    private function buildBody(?BodyDtoInterface $body): ?string
    {
        return $body ? $this->jsonObjectMapper->serialize($body) : null;
    }
}
