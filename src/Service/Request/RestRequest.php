<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\Request;

use Sun\TransportBookingDto\Request\BodyDtoInterface;
use Sun\TransportBookingDto\Request\QueryDtoInterface;
use Sun\TransportBookingDto\Response\ResponseDtoInterface;

class RestRequest extends AbstractRequest
{
    /**
     * @template T of ResponseDtoInterface
     * @param string $endPoint
     * @param class-string<T>|null $returnType
     * @param QueryDtoInterface|null $query
     * @param BodyDtoInterface|null $body
     */
    public function __construct(
        string $endPoint,
        protected ?string $returnType = null,
        ?QueryDtoInterface $query = null,
        ?BodyDtoInterface $body = null,
    ) {
        parent::__construct($endPoint, $query, $body);
    }

    public function getReturnType(): ?string
    {
        return $this->returnType;
    }
}
