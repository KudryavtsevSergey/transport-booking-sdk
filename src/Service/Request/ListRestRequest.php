<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\Request;

class ListRestRequest extends RestRequest
{
    private const LIST_POSTFIX = '[]';

    public function getReturnType(): ?string
    {
        return $this->returnType !== null ? sprintf('%s%s', $this->returnType, self::LIST_POSTFIX) : null;
    }
}
