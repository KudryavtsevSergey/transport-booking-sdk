<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\Authentication;

class AuthenticationClient
{
    public function __construct(
        private string $clientId,
        private string $clientSecret,
        private string $urlAccess,
    ) {
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function getUrlAccess(): string
    {
        return $this->urlAccess;
    }

    public function getUrlAccessToken(): string
    {
        return sprintf('%s/oauth/token', $this->urlAccess);
    }

    public function getUrlAuthorize(): string
    {
        return sprintf('%s/oauth/authorize', $this->urlAccess);
    }

    public function getUrlResourceOwnerDetails(): string
    {
        return sprintf('%s/oauth/resource', $this->urlAccess);
    }
}
