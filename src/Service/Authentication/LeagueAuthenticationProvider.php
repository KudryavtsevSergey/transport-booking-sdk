<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\Authentication;

use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Sun\TransportBookingSdk\Exceptions\OAuth2CreateTokenException;
use Throwable;

class LeagueAuthenticationProvider implements AuthenticationProvider
{
    private const PASSWORD_GRANT = 'password';
    private const REFRESH_TOKEN_GRANT = 'refresh_token';

    private GenericProvider $provider;

    public function __construct(AuthenticationClient $client)
    {
        $this->provider = new GenericProvider([
            'clientId' => $client->getClientId(),
            'clientSecret' => $client->getClientSecret(),
            'urlAccessToken' => $client->getUrlAccessToken(),
            'urlAuthorize' => $client->getUrlAuthorize(),
            'urlResourceOwnerDetails' => $client->getUrlResourceOwnerDetails(),
        ]);
    }

    public function createAccessToken(ApiUser $apiUser): AccessTokenInterface
    {
        try {
            return $this->provider->getAccessToken(self::PASSWORD_GRANT, [
                'username' => $apiUser->getUsername(),
                'password' => $apiUser->getPassword(),
            ]);
        } catch (IdentityProviderException|Throwable $exception) {
            throw new OAuth2CreateTokenException($exception);
        }
    }

    public function refreshAccessToken(string $refreshToken): AccessTokenInterface
    {
        try {
            return $this->provider->getAccessToken(self::REFRESH_TOKEN_GRANT, [
                'refresh_token' => $refreshToken,
            ]);
        } catch (IdentityProviderException|Throwable $exception) {
            throw new OAuth2CreateTokenException($exception);
        }
    }
}
