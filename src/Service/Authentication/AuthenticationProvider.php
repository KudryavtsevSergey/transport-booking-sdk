<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Service\Authentication;

use League\OAuth2\Client\Token\AccessTokenInterface;

interface AuthenticationProvider
{
    public function createAccessToken(ApiUser $apiUser): AccessTokenInterface;

    public function refreshAccessToken(string $refreshToken): AccessTokenInterface;
}
