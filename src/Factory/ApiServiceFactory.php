<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Factory;

use Sun\TransportBookingSdk\Api\ApiService;
use Sun\TransportBookingSdk\Mapper\ArrayObjectMapper;
use Sun\TransportBookingSdk\Mapper\JsonObjectMapper;
use Sun\TransportBookingSdk\Service\HttpClient\GuzzleHttpClient;
use Sun\TransportBookingSdk\Service\Request\RequestService;

class ApiServiceFactory
{
    private const BEARER_TYPE = 'Bearer';
    private const LOCALIZATION_HEADER_KEY = 'X-LOCALIZATION';
    private const AUTHORIZATION_HEADER_KEY = 'Authorization';

    public function __construct(
        private string $uri,
        private string $locale,
    ) {
    }

    public function createApiService(string $accessToken): ApiService
    {
        $client = new GuzzleHttpClient($this->uri, [
            self::AUTHORIZATION_HEADER_KEY => sprintf('%s %s', self::BEARER_TYPE, $accessToken),
            self::LOCALIZATION_HEADER_KEY => $this->locale,
        ]);
        $requestService = new RequestService($client, new ArrayObjectMapper(), new JsonObjectMapper());
        return new ApiService($requestService);
    }
}
