<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Factory;

use League\OAuth2\Client\Token\AccessTokenInterface;
use Sun\TransportBookingSdk\Service\Authentication\ApiUser;
use Sun\TransportBookingSdk\Service\Authentication\AuthenticationClient;
use Sun\TransportBookingSdk\Service\Authentication\AuthenticationProvider;
use Sun\TransportBookingSdk\Service\Authentication\LeagueAuthenticationProvider;

class TokenFactory
{
    private AuthenticationProvider $provider;

    public function __construct(
        private ApiUser $apiUser,
        AuthenticationClient $authenticationClient,
    ) {
        $this->provider = new LeagueAuthenticationProvider($authenticationClient);
    }

    public function createAccessToken(): AccessTokenInterface
    {
        return $this->provider->createAccessToken($this->apiUser);
    }

    public function refreshAccessToken(string $refreshToken): AccessTokenInterface
    {
        return $this->provider->refreshAccessToken($refreshToken);
    }
}
