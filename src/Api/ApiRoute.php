<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Api;

final class ApiRoute
{
    public const LOCALES = '/api/data/locales/all';
    public const COUNTRIES = '/api/data/countries/all';
    public const CURRENCIES = '/api/data/currencies/all';
    public const DOCUMENT_TYPES = '/api/data/documentTypes/all';
    public const TICKET_TYPES = '/api/data/ticketTypes/all';
    public const SOCIAL_PROVIDERS = '/api/data/socialProviders/all';
    public const PASSENGERS = '/api/data/passengers/all';
    public const ROUTES = '/api/data/routes/all';
    public const ROUTE_WITH_STOPS = '/api/data/routes/%s/stops';
    public const JOURNEY_DATES_SCHEDULE = '/api/data/journey_dates/schedule';
    public const ORDERS = '/api/data/orders';
    public const ORDERS_LIST = '/api/data/orders/all';
    public const ORDER = '/api/data/orders/%s';
    public const CANCEL_ORDER = '/api/data/orders/%s/cancel';
    public const CANCEL_TICKET = '/api/data/orders/%s/tickets/%s/cancel';
    public const CANCEL_BAGGAGE = '/api/data/orders/%s/baggage/%s/cancel';
    public const CANCEL_TICKET_SERVICE = '/api/data/orders/%s/ticket_services/%s/cancel';
    public const MAKE_PAYMENT_LINK = '/api/data/orders/%s/paymenttypes/%s/link';
    public const AGENT_CUSTOMERS = '/api/data/agent_customers';
    public const AGENT_CUSTOMERS_LIST = '/api/data/agent_customers/all';
    public const AGENT_CUSTOMER_DETAIL = '/api/data/agent_customers/%s';
    public const AGENT_CUSTOMER_EDIT = '/api/data/agent_customers/%s/edit';

    public const DEPARTURE_STOPS = '/api/booking/getDepartureStops';
    public const ARRIVAL_STOPS = '/api/booking/getArrivalStops/%s';
    public const OFFERS = '/api/booking/getOffers/%s/%s';
    public const JOURNEY_DATES = '/api/booking/getJourneyDates/%s/%s';
    public const HOLD = '/api/booking/createHold';
    public const HOLD_INFO = '/api/booking/getHoldInfo/%s';
    public const CANCEL_HOLD = '/api/booking/cancelHold/%s';
    public const CREATE_ORDER = '/api/booking/createOrder/%s';
    public const APPLY_PROMO_CODE = '/api/booking/holds/%s/applyPromoCode/%s';
    public const DELETE_PROMO_CODE = '/api/booking/holds/%s/deletePromoCode/%s';
    public const GET_AVAILABLE_NOTIFICATION_PROVIDERS = '/api/booking/getAvailableNotificationProviders';
    public const DOWNLOAD_ORDER_TICKETS = '/api/data/orders/%s/tickets/download';
    public const SETTINGS = '/api/data/settings';
    public const FILES = '/api/files/%s/%s';
    public const UPDATE_PASSENGER = '/api/data/passengers/%s';
}
