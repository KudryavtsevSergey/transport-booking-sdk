<?php

declare(strict_types=1);

namespace Sun\TransportBookingSdk\Api;

use GuzzleHttp\Promise\PromiseInterface;
use Sun\TransportBookingDto\Request\Booking\CustomerRequestDto;
use Sun\TransportBookingDto\Request\Booking\HoldRequestDto;
use Sun\TransportBookingDto\Request\Booking\OffersRequestDto;
use Sun\TransportBookingDto\Request\Booking\OrderRequestDto;
use Sun\TransportBookingDto\Request\Booking\PromoCodeRequestDto;
use Sun\TransportBookingDto\Request\Booking\RegisterCustomerRequestDto;
use Sun\TransportBookingDto\Request\Filter\CountryFilter;
use Sun\TransportBookingDto\Request\Filter\CurrencyFilter;
use Sun\TransportBookingDto\Request\Filter\CustomerFilter;
use Sun\TransportBookingDto\Request\Filter\DocumentTypeFilter;
use Sun\TransportBookingDto\Request\Filter\JourneyDateFilter;
use Sun\TransportBookingDto\Request\Filter\LocaleFilter;
use Sun\TransportBookingDto\Request\Filter\OrderFilter;
use Sun\TransportBookingDto\Request\Filter\PassengerFilter;
use Sun\TransportBookingDto\Request\Filter\RouteFilter;
use Sun\TransportBookingDto\Request\Filter\SearchDto;
use Sun\TransportBookingDto\Request\Filter\SocialProviderFilter;
use Sun\TransportBookingDto\Request\Filter\TicketTypeFilter;
use Sun\TransportBookingDto\Request\FullPassengerRequestDto;
use Sun\TransportBookingDto\Request\PageRequestDto;
use Sun\TransportBookingDto\Request\PaginationDto;
use Sun\TransportBookingDto\Request\PaymentLinkRequestDto;
use Sun\TransportBookingDto\Response\Booking\CurrencyDto;
use Sun\TransportBookingDto\Response\Booking\CustomerResponseDto;
use Sun\TransportBookingDto\Response\Booking\CustomerPageResponseDto;
use Sun\TransportBookingDto\Response\Booking\HoldInfoDto;
use Sun\TransportBookingDto\Response\Booking\JourneyDateDto;
use Sun\TransportBookingDto\Response\Booking\JourneyHoldSeatDto;
use Sun\TransportBookingDto\Response\Booking\OfferDto;
use Sun\TransportBookingDto\Response\Booking\Order\BaggageDto;
use Sun\TransportBookingDto\Response\Booking\Order\JourneyTicketDto;
use Sun\TransportBookingDto\Response\Booking\Order\PayableOrderDto;
use Sun\TransportBookingDto\Response\Booking\Order\ShortOrderDto;
use Sun\TransportBookingDto\Response\Booking\Order\TicketServiceDto;
use Sun\TransportBookingDto\Response\Booking\OrderPageResponseDto;
use Sun\TransportBookingDto\Response\Booking\StopDto;
use Sun\TransportBookingDto\Response\CountryDto;
use Sun\TransportBookingDto\Response\DocumentTypeDto;
use Sun\TransportBookingDto\Response\JourneyDateScheduleResponseDto;
use Sun\TransportBookingDto\Response\LocaleDto;
use Sun\TransportBookingDto\Response\NotificationProviderDto;
use Sun\TransportBookingDto\Response\PassengerDto;
use Sun\TransportBookingDto\Response\PaymentLinkResponseDto;
use Sun\TransportBookingDto\Response\RouteDto;
use Sun\TransportBookingDto\Response\RouteWithStopsDto;
use Sun\TransportBookingDto\Response\SettingsDto;
use Sun\TransportBookingDto\Response\SocialProviderDto;
use Sun\TransportBookingDto\Response\TicketTypeDto;
use Sun\TransportBookingSdk\Service\Request\FileRequest;
use Sun\TransportBookingSdk\Service\Request\ListRestRequest;
use Sun\TransportBookingSdk\Service\Request\RequestService;
use Sun\TransportBookingSdk\Service\Request\RestRequest;

class ApiService
{
    public function __construct(
        private RequestService $requestService,
    ) {
    }

    public function listLocales(LocaleFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::LOCALES, LocaleDto::class, new SearchDto($filter)
        ));
    }

    public function listCountries(CountryFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::COUNTRIES, CountryDto::class, new SearchDto($filter)
        ));
    }

    public function listCurrencies(CurrencyFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::CURRENCIES, CurrencyDto::class, new SearchDto($filter)
        ));
    }

    public function listDocumentTypes(DocumentTypeFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::DOCUMENT_TYPES, DocumentTypeDto::class, new SearchDto($filter)
        ));
    }

    // TODO: not used
    public function listTicketTypes(TicketTypeFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::TICKET_TYPES, TicketTypeDto::class, new SearchDto($filter)
        ));
    }

    public function listSocialProviders(SocialProviderFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::SOCIAL_PROVIDERS, SocialProviderDto::class, new SearchDto($filter)
        ));
    }

    public function listPassengers(PassengerFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::PASSENGERS, PassengerDto::class, new SearchDto($filter)
        ));
    }

    public function listRoutes(RouteFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::ROUTES, RouteDto::class, new SearchDto($filter)
        ));
    }

    public function getRouteWithStops(int $routeId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::ROUTE_WITH_STOPS, $routeId);
        return $this->requestService->sendGetRequest(new RestRequest(
            $endPoint, RouteWithStopsDto::class
        ));
    }

    public function journeyDatesSchedule(?JourneyDateFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::JOURNEY_DATES_SCHEDULE, JourneyDateScheduleResponseDto::class, new SearchDto($filter)
        ));
    }

    public function pageOrders(PaginationDto $pagination, ?OrderFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new RestRequest(
            ApiRoute::ORDERS, OrderPageResponseDto::class, new PageRequestDto($pagination, $filter)
        ));
    }

    public function listOrders(OrderFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::ORDERS_LIST, ShortOrderDto::class, new SearchDto($filter)
        ));
    }

    public function getOrder(int $orderId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::ORDER, $orderId);
        return $this->requestService->sendGetRequest(new RestRequest(
            $endPoint, PayableOrderDto::class
        ));
    }

    public function cancelOrder(int $orderId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::CANCEL_ORDER, $orderId);
        return $this->requestService->sendPutRequest(new RestRequest(
            $endPoint, PayableOrderDto::class
        ));
    }

    public function cancelOrderTicket(int $orderId, int $ticketId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::CANCEL_TICKET, $orderId, $ticketId);
        return $this->requestService->sendPutRequest(new RestRequest(
            $endPoint, JourneyTicketDto::class
        ));
    }

    public function cancelOrderBaggage(int $orderId, int $baggageId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::CANCEL_BAGGAGE, $orderId, $baggageId);
        return $this->requestService->sendPutRequest(new RestRequest(
            $endPoint, BaggageDto::class
        ));
    }

    public function cancelOrderTicketService(int $orderId, int $ticketServiceId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::CANCEL_TICKET_SERVICE, $orderId, $ticketServiceId);
        return $this->requestService->sendPutRequest(new RestRequest(
            $endPoint, TicketServiceDto::class
        ));
    }

    public function listDepartureStops(): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::DEPARTURE_STOPS, StopDto::class
        ));
    }

    public function listArrivalStops(int $departureStopId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::ARRIVAL_STOPS, $departureStopId);
        return $this->requestService->sendGetRequest(new ListRestRequest(
            $endPoint, StopDto::class
        ));
    }

    public function getJourneyDate(int $departureStopId, int $arrivalStopId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::JOURNEY_DATES, $departureStopId, $arrivalStopId);
        return $this->requestService->sendGetRequest(new RestRequest(
            $endPoint, JourneyDateDto::class
        ));
    }

    public function listOffers(int $departureStopId, int $arrivalStopId, OffersRequestDto $query): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::OFFERS, $departureStopId, $arrivalStopId);
        return $this->requestService->sendGetRequest(new ListRestRequest(
            $endPoint, OfferDto::class, $query
        ));
    }

    public function createHold(HoldRequestDto $request): PromiseInterface
    {
        return $this->requestService->sendPostRequest(new RestRequest(
            ApiRoute::HOLD, HoldInfoDto::class, body: $request
        ));
    }

    public function getHoldInfo(int $holdId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::HOLD_INFO, $holdId);
        return $this->requestService->sendGetRequest(new RestRequest(
            $endPoint, HoldInfoDto::class
        ));
    }

    public function cancelHold(int $holdId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::CANCEL_HOLD, $holdId);
        return $this->requestService->sendDeleteRequest(new RestRequest($endPoint));
    }

    public function pageAgentCustomers(PaginationDto $pagination, ?CustomerFilter $filter = null): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new RestRequest(
            ApiRoute::AGENT_CUSTOMERS, CustomerPageResponseDto::class, new PageRequestDto($pagination, $filter)
        ));
    }

    public function getAgentCustomers(): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::AGENT_CUSTOMERS_LIST, CustomerResponseDto::class
        ));
    }

    public function createOrder(int $holdId, OrderRequestDto $request): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::CREATE_ORDER, $holdId);
        return $this->requestService->sendPostRequest(new RestRequest(
            $endPoint, PayableOrderDto::class, body: $request
        ));
    }

    public function applyPromoCode(int $holdId, int $holdSeatId, PromoCodeRequestDto $request): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::APPLY_PROMO_CODE, $holdId, $holdSeatId);
        return $this->requestService->sendPutRequest(new RestRequest(
            $endPoint, JourneyHoldSeatDto::class, null, $request
        ));
    }

    public function deletePromoCode(int $holdId, int $holdSeatId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::DELETE_PROMO_CODE, $holdId, $holdSeatId);
        return $this->requestService->sendDeleteRequest(new RestRequest($endPoint));
    }

    public function downloadTicketsPdfByOrderId(int $orderId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::DOWNLOAD_ORDER_TICKETS, $orderId);
        return $this->requestService->sendFileRequest(new FileRequest($endPoint));
    }

    public function downloadFile(string $bucket, string $fileId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::FILES, $bucket, $fileId);
        return $this->requestService->sendFileRequest(new FileRequest($endPoint));
    }

    public function getAvailableNotificationProviders(): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new ListRestRequest(
            ApiRoute::GET_AVAILABLE_NOTIFICATION_PROVIDERS, NotificationProviderDto::class
        ));
    }

    public function makePaymentLink(int $orderId, string $paymentType, PaymentLinkRequestDto $request): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::MAKE_PAYMENT_LINK, $orderId, $paymentType);
        return $this->requestService->sendGetRequest(new RestRequest(
            $endPoint, PaymentLinkResponseDto::class, $request
        ));
    }

    public function getSettings(): PromiseInterface
    {
        return $this->requestService->sendGetRequest(new RestRequest(
            ApiRoute::SETTINGS, SettingsDto::class
        ));
    }

    public function updatePassenger(int $passengerId, FullPassengerRequestDto $request): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::UPDATE_PASSENGER, $passengerId);
        return $this->requestService->sendPutRequest(new RestRequest(
            $endPoint, PassengerDto::class, body: $request
        ));
    }

    public function createAgentCustomer(RegisterCustomerRequestDto $request): PromiseInterface
    {
        return $this->requestService->sendPostRequest(new RestRequest(
            ApiRoute::AGENT_CUSTOMERS, CustomerResponseDto::class, body: $request
        ));
    }

    public function getAgentCustomer(int $customerId): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::AGENT_CUSTOMER_EDIT, $customerId);
        return $this->requestService->sendGetRequest(new RestRequest(
            $endPoint, CustomerResponseDto::class
        ));
    }

    public function updateAgentCustomer(int $customerId, CustomerRequestDto $request): PromiseInterface
    {
        $endPoint = sprintf(ApiRoute::AGENT_CUSTOMER_DETAIL, $customerId);
        return $this->requestService->sendPutRequest(new RestRequest(
            $endPoint, CustomerResponseDto::class, body: $request
        ));
    }
}
